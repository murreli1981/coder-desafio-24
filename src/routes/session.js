const {
  login,
  ingreso,
  createSession,
  logout,
  auth,
} = require("../controllers/session");

module.exports = (router) => {
  router.get("/ingreso", auth, ingreso);
  router.get("/login", login);
  router.post("/login", createSession);
  router.get("/logout", logout);
  return router;
};
