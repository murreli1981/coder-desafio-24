const {
  listProducts,
  createProduct,
  getProduct,
  updateProduct,
  deleteProduct,
} = require("../controllers/product");
const { auth } = require("../controllers/session");
module.exports = (router) => {
  router.get("/api/productos", auth, listProducts);
  router.get("/api/productos/:id", auth, getProduct);
  router.post("/api/productos", auth, createProduct);
  router.put("/api/productos/:id", auth, updateProduct);
  router.delete("/api/productos/:id", auth, deleteProduct);
  return router;
};
