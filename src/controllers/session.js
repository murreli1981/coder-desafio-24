exports.auth = async (req, res, next) => {
  if (req.session.isLogged) {
    req.session.isLogged = req.session.isLogged;
    next();
  } else {
    console.info("session expired");
    res.redirect("/login");
  }
};

exports.createSession = async (req, res, next) => {
  console.log(req.body);
  req.session.isLogged = true;
  const { username } = req.body;
  res.cookie("username", username);
  res.redirect("/ingreso");
};

exports.login = async (req, res, next) => {
  res.render("login");
};

exports.ingreso = async (req, res, next) => {
  res.render("input", { username: req.cookies.username });
};

exports.logout = async (req, res, next) => {
  console.log(req.session);
  req.session.destroy((err) => console.log("session destroyed"));
  console.log(req.session);
  res.send("chau!");
};
