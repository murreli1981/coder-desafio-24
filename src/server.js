const http = require("http");
const express = require("express");
const app = express();
const server = http.createServer(app);
const router = express.Router();
const session = require("express-session");
const cookieParser = require("cookie-parser");

const { Server, client } = require("socket.io");
const io = new Server(server);

const { PORT } = require("./config/globals");

const productRoutes = require("./routes/product");
const sessionRoutes = require("./routes/session");
const { getConnection } = require("./dao/db/connection");

app.set("views", "./src/views");
app.set("view engine", "ejs");

const sessionMiddleware = session({
  secret: `${process.env.SECRET_KEY}`,
  resave: true,
  saveUninitialized: true,
  rolling: true,
  cookie: {
    maxAge: 20 * 1000,
  },
});

app.use(sessionMiddleware);

//paso el socket
app.use((req, res, next) => {
  req.io = io;
  next();
});
app.use(cookieParser());

app.use(express.json());
app.use(express.urlencoded());

app.use("/public", express.static("./src/resources"));

app.use(sessionRoutes(router));
app.use(productRoutes(router));

getConnection().then(() =>
  server.listen(PORT, () => console.log("server's up", PORT))
);

io.on("connection", async (socket) => {
  const ProductService = require("./services/product");
  const MessageService = require("./services/message");
  productService = new ProductService();
  messageService = new MessageService();

  console.log("new connection", socket.id);

  io.sockets.emit("list:products", await productService.getAllProducts());
  io.sockets.emit("chat:messages", await messageService.getAllMessages());

  socket.on("chat:new-message", async (data) => {
    const { author, message } = data;
    const entry = {
      author,
      message,
    };
    console.log(entry);
    await messageService.addMessage(entry);
    io.sockets.emit("chat:messages", await messageService.getAllMessages());
  });
});
