# coder-desafio-24

Normalizr

## endpoints:

[GET]/api/productos - Lista todos los productos

[GET]/api/productos/:id - Trae un único producto por id

[POST] /api/productos - Guarda un producto en la base

[PUT] /api/productos/:id - Modifica un producto por id

[DELETE] /api/productos/:id - Elimina un producto por id

## UI

/ingreso

/login

/logout
